console.log('hello')

let getCube = 2**3
console.log(`the cube of a 2 ${getCube}`)

let address = [
	'brgy. Uppercalerian',
	'brgy. canelar',
	'brgy. putik']


console.log(`i live at this places in ${address[0]}, ${address[1]}, and ${address[2]}`)

let animal = {
	dog: 'brave',
	cat: 'cute,',
	rabbit : 'fluggy',
}

const{dog, cat, rabbit} = animal;
console.log(`a dog is ${dog},a cat is ${cat}, and a rabbit is ${rabbit}`)

const students = ["John", "Jane", "Jasmine"];

students.forEach((student) => {
	console.log(`${student} is a student.`)
})


const numbers = [1, 2, 3, 4];

const initialValue = 0;
const sumWithInitial = numbers.reduce(
  (previousValue, currentValue) => previousValue + currentValue,
  initialValue
);
console.log(sumWithInitial)

function Dog (name, age, breed){
	this.name = name;
	this.age = age; 
	this.breed =  breed;
}

let bernie = new Dog ('bernie', 2, 'husky');

console.log(bernie);

